from base.TestBase import TestBase
import unittest
from loguru import logger
from ddt import ddt, data, unpack
from base.BaseFunctions import *  # get, post, readFile
from base.URLBase import * #导入所有的url


@ddt
class TestAddAdmin(TestBase):
    @data(*(readFile("TestAddAdmin.xlsx")))
    @unpack
    def test_AddAdmin(self,id, loginAccount, loginPwd, account, birthday, card, cityId, companyId, departmentId,
                            email, name, phone, productId, pwd, roles, sex, status, message, description):
        logger.info(f"************************{id}：{description}*****************************")
        ######################################初始化环境，造数据等######################################
        deleteAdmin(name)
        deleteUser(account)

        #获取验证码
        response = get(codevilCodeURL)
        cookie= response.cookies.get_dict()["JSESSIONID"]
        # 登录
        data = {}
        data["account"] = loginAccount
        data["password"] = str(loginPwd)
        data["authCode"] = "8888"

        headers={"Cookie": f"JSESSIONID={cookie}"}
        result = post(loginURL, data, headers)
        token = result["data"]["access_token"]
        logger.info(token)

        ######################################   组装接口并请求  ######################################
        data.clear()
        data["account"] =account
        data["birthday"] =birthday
        data["card"] =card
        data["cityId"] =cityId
        data["companyId"] =companyId
        data["departmentId"] =departmentId
        data["email"] =email
        data["name"] =name
        data["phone"] =phone
        data["productId"] =productId
        data["pwd"] =pwd
        rolesList=str(roles).split(",")
        data["roles"] =rolesList
        data["sex"] =sex

        headers.clear()
        headers["Authorization"] = "Bearer"+token
        response = postJSON(addAdminURL, data, headers)

        #验证response
        self.assertEqual(status, response["status"])
        self.assertEqual(message, response["userMessage"])

        #验证数据库
        if id == 1:
            admin = selectAdmin(name)
            self.assertEqual(name, admin.name)
            self.assertEqual(account, admin.account)
            self.assertEqual(birthday, admin.birthday.strftime("%Y-%m-%d"))
            self.assertEqual(str(card), admin.card)
            self.assertEqual(cityId, admin.city_id)
            self.assertEqual(int(companyId), admin.company_id)
            self.assertEqual(departmentId, admin.department_id)
            self.assertEqual(email, admin.email)
            self.assertEqual(str(phone), admin.phone)
            self.assertEqual(int(productId), admin.product_id)
            self.assertIsNone(admin.contract_time)
            self.assertIsNotNone(admin.create_time)
            self.assertIsNone(admin.entry_time)
            self.assertIsNone(admin.last_login_time)
            self.assertIsNone(admin.leave_time)
            self.assertIsNone(admin.out_date)
            self.assertIsNone(admin.positive_time)
            self.assertIsNone(admin.reset_pwd_time)
            self.assertEqual('0', admin.schedule)
            self.assertEqual(sex, admin.sex)
            self.assertEqual(1, admin.status)
            self.assertIsNone(admin.validata_code)

            user = selectUser(account)
            self.assertEqual(account, user.username)
            self.assertEqual(int(companyId), user.company_id)
            self.assertIsNone(user.new_password)
            self.assertIsNone(user.prompt_message)



        ######################################还原环境，清理数据等######################################
        deleteAdmin(name)
        deleteUser(account)

if __name__ == '__main__':
    unittest.main()




















































