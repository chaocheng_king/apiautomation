import unittest
from packages.HTMLTestRunner import HTMLTestRunner
import time

discover = unittest.defaultTestLoader.discover("./",pattern='TestAdd*.py')

now = time.strftime("%Y-%m-%d %H_%M_%S")

filename = "../report/" + now + "_result.html"

fp = open(filename, "wb")

runner = HTMLTestRunner(stream=fp, title="测试报告", description="测试执行情况统计", verbosity=2)

runner.run(discover)



