from base.TestBase import TestBase
import unittest
from loguru import logger
from ddt import ddt, data, unpack
from base.BaseFunctions import *  # get, post, readFile
from base.URLBase import * #导入所有的url


@ddt
class TestAPINameExample(TestBase):
    @data(*(readFile("TestAPINameExample.xlsx")))
    @unpack
    def test_APINameExample(self, id, description):
        logger.info(f"************************{id}：{description}*****************************")
        ######################################初始化环境，造数据等######################################


        ######################################   组装接口并请求  ######################################


        ######################################还原环境，清理数据等######################################


if __name__ == '__main__':
    unittest.main()




















































