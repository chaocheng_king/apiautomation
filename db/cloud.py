from peewee import *
from db.db_Base import *

database = MySQLDatabase('cloud', **{'charset': 'utf8', 'sql_mode': 'PIPES_AS_CONCAT', 'use_unicode': True,
'host': host, 'port': port, 'user': user, 'password': password})

class UnknownField(object):
    def __init__(self, *_, **__): pass

class BaseModel(Model):
    class Meta:
        database = database

class Admin(BaseModel):
    account = CharField(unique=True)
    add_id = IntegerField(null=True)
    birthday = DateField(null=True)
    card = CharField(null=True)
    city_id = IntegerField(index=True, null=True)
    company_id = IntegerField(constraints=[SQL("DEFAULT 0")], null=True)
    contract_time = DateField(null=True)
    create_time = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")], null=True)
    department_id = IntegerField(index=True, null=True)
    email = CharField(null=True)
    entry_time = DateField(null=True)
    last_login_time = DateTimeField(null=True)
    leave_time = DateField(null=True)
    name = CharField(null=True)
    out_date = DateTimeField(null=True)
    phone = CharField(null=True)
    positive_time = DateField(null=True)
    product_id = IntegerField(null=True)
    reset_pwd_time = DateField(null=True)
    schedule = CharField(constraints=[SQL("DEFAULT '0'")], null=True)
    sex = CharField(null=True)
    status = IntegerField(constraints=[SQL("DEFAULT 1")], null=True)
    validata_code = CharField(null=True)

    class Meta:
        table_name = 'admin'

class CloudBaseRecruit(BaseModel):
    city_id = IntegerField(null=True)
    city_name = CharField(null=True)
    company_id = IntegerField(null=True)
    company_name = CharField(null=True)
    name = CharField(null=True)
    phone = CharField(null=True)
    product_id = IntegerField(null=True)
    product_name = CharField(null=True)

    class Meta:
        table_name = 'cloud_base_recruit'

class CloudChangeLog(BaseModel):
    create_time = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")], null=True)
    new_user_id = IntegerField(constraints=[SQL("DEFAULT 0")], null=True)
    new_user_name = CharField(null=True)
    old_user_id = IntegerField(constraints=[SQL("DEFAULT 0")], null=True)
    old_user_name = CharField(null=True)
    operator = CharField(null=True)
    operator_id = IntegerField(constraints=[SQL("DEFAULT 0")], null=True)
    progress_state = IntegerField(null=True)
    reason = CharField(null=True)
    recruit_id = IntegerField(constraints=[SQL("DEFAULT 0")], null=True)

    class Meta:
        table_name = 'cloud_change_log'

class CloudChannel(BaseModel):
    channel_id = IntegerField(null=True)
    create_time = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")], null=True)
    name = CharField(null=True)
    type = IntegerField(null=True)

    class Meta:
        table_name = 'cloud_channel'

class CloudCity(BaseModel):
    city = CharField(null=True)
    create_time = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")], null=True)

    class Meta:
        table_name = 'cloud_city'

class CloudCompany(BaseModel):
    city_id = IntegerField(index=True, null=True)
    company = CharField(null=True)
    create_time = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")], null=True)

    class Meta:
        table_name = 'cloud_company'

class CloudConfig(BaseModel):
    company_id = IntegerField(constraints=[SQL("DEFAULT 0")], null=True)
    config = IntegerField(null=True)
    create_time = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")], null=True)
    last_modify_time = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")], null=True)
    many_time = IntegerField(null=True)

    class Meta:
        table_name = 'cloud_config'

class CloudDepartment(BaseModel):
    city_id = IntegerField(null=True)
    create_time = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")], null=True)
    department = CharField(null=True)
    pid = IntegerField(null=True)
    product_id = IntegerField(null=True)
    type = IntegerField(constraints=[SQL("DEFAULT 1")], null=True)
    user_id = IntegerField(null=True)

    class Meta:
        table_name = 'cloud_department'

class CloudEmail(BaseModel):
    city_id = IntegerField(index=True, null=True)
    company_id = IntegerField(index=True, null=True)
    create_time = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")], null=True)
    delete_id = IntegerField(null=True)
    email = CharField(null=True)
    last_modify_time = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")], null=True)
    points_id = IntegerField(constraints=[SQL("DEFAULT 0")], null=True)
    product_id = IntegerField(index=True, null=True)
    pwd = CharField(null=True)
    remove = IntegerField(constraints=[SQL("DEFAULT 1")], null=True)
    status = IntegerField(null=True)
    user_id = IntegerField(null=True)

    class Meta:
        table_name = 'cloud_email'

class CloudEveryPriority(BaseModel):
    create_time = DateField(null=True)
    priority = IntegerField(constraints=[SQL("DEFAULT 0")], null=True)
    recruit_id = AutoField()
    state = IntegerField(constraints=[SQL("DEFAULT 0")], null=True)
    user_id = IntegerField(null=True)
    visit_num = IntegerField(null=True)

    class Meta:
        table_name = 'cloud_every_priority'

class CloudProduct(BaseModel):
    create_time = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")], null=True)
    last_modify_time = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")], null=True)
    product = CharField(null=True)
    user_id = IntegerField(null=True)

    class Meta:
        table_name = 'cloud_product'

class CloudRecruit(BaseModel):
    age = IntegerField(null=True)
    channel_id = IntegerField(index=True, null=True)
    city_id = IntegerField(index=True, null=True)
    company_id = IntegerField(constraints=[SQL("DEFAULT 0")], null=True)
    create_time = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")], null=True)
    degree = IntegerField(null=True)
    distribute_state = IntegerField(constraints=[SQL("DEFAULT 0")], null=True)
    distribute_time = DateTimeField(null=True)
    email = CharField(null=True)
    input_id = IntegerField(null=True)
    input_name = CharField(null=True)
    jobs = CharField(null=True)
    last_modify_time = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")], null=True)
    major = CharField(null=True)
    name = CharField(null=True)
    next_track_time = DateTimeField(index=True, null=True)
    note = TextField(null=True)
    old_user_id = IntegerField(null=True)
    phone = CharField(index=True, null=True)
    points_id = IntegerField(null=True)
    priority = IntegerField(null=True)
    progress_state = IntegerField(null=True)
    qq = CharField(null=True)
    repeat_time = DateTimeField(null=True)
    repetition = IntegerField(constraints=[SQL("DEFAULT 1")], null=True)
    sex = CharField(null=True)
    source_id = IntegerField(index=True, null=True)
    source_time = DateTimeField(null=True)
    source_type = IntegerField(null=True)
    tail_num = IntegerField(constraints=[SQL("DEFAULT 0")], null=True)
    track_time = DateTimeField(null=True)
    train_type = IntegerField(null=True)
    user_id = IntegerField(null=True)
    user_name = CharField(null=True)
    weixin = CharField(null=True)

    class Meta:
        table_name = 'cloud_recruit'
        indexes = (
            (('company_id', 'train_type'), False),
            (('company_id', 'train_type', 'priority', 'distribute_state', 'distribute_time'), False),
            (('progress_state', 'company_id', 'train_type', 'distribute_state', 'distribute_time'), False),
            (('user_id', 'distribute_state', 'distribute_time', 'progress_state'), False),
            (('user_id', 'distribute_state', 'priority', 'distribute_time'), False),
        )

class CloudRecruitDetail(BaseModel):
    address = CharField(null=True)
    birthday = DateField(null=True)
    card = CharField(null=True)
    create_time = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")], null=True)
    email_id = IntegerField(null=True)
    graduate_time = DateField(null=True)
    recruit_id = AutoField()
    school = CharField(null=True)
    unified = CharField(null=True)
    visit_id = IntegerField(null=True)
    visit_time = DateTimeField(null=True)

    class Meta:
        table_name = 'cloud_recruit_detail'

class CloudRecruitDetailResume(BaseModel):
    content = TextField(null=True)
    create_time = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")], null=True)
    recruit_id = IntegerField(null=True)
    resume_id = IntegerField(null=True, unique=True)
    status = IntegerField(null=True)

    class Meta:
        table_name = 'cloud_recruit_detail_resume'
        indexes = (
            (('recruit_id', 'status'), False),
        )

class CloudRecruitResume(BaseModel):
    create_time = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")], null=True)
    jobs = CharField(null=True)
    recruit_id = IntegerField(constraints=[SQL("DEFAULT 0")], null=True)
    recruit_name = CharField(null=True)
    source_id = IntegerField(null=True)
    source_time = DateTimeField(null=True)
    status = IntegerField(null=True)
    title = CharField(null=True)

    class Meta:
        table_name = 'cloud_recruit_resume'

class CloudRepeatDeliveryTerm(BaseModel):
    create_by = CharField(null=True)
    create_time = DateTimeField(null=True)
    repeat_term = IntegerField(null=True)
    update_time = DateTimeField(null=True)

    class Meta:
        table_name = 'cloud_repeat_delivery_term'

class CloudTracking(BaseModel):
    create_time = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")], null=True)
    give_up = IntegerField(null=True)
    last_modify_time = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")], index=True, null=True)
    next_time = DateTimeField(null=True)
    recruit_id = IntegerField(constraints=[SQL("DEFAULT 0")], index=True, null=True)
    track_content = CharField(null=True)
    track_length = IntegerField(constraints=[SQL("DEFAULT 0")], null=True)
    track_result = IntegerField(constraints=[SQL("DEFAULT 1")], null=True)
    track_time = DateTimeField(index=True, null=True)
    track_type = IntegerField(constraints=[SQL("DEFAULT 1")], null=True)
    user_id = IntegerField(constraints=[SQL("DEFAULT 0")], null=True)
    user_name = CharField(null=True)

    class Meta:
        table_name = 'cloud_tracking'
        indexes = (
            (('track_time', 'user_id'), False),
        )

class CloudUserDistribute(BaseModel):
    create_time = DateField(null=True)
    distribute = IntegerField(null=True)
    user_id = IntegerField(null=True)

    class Meta:
        table_name = 'cloud_user_distribute'

class Role(BaseModel):
    id = BigAutoField()
    name = CharField()

    class Meta:
        table_name = 'role'

class User(BaseModel):
    company_id = IntegerField(constraints=[SQL("DEFAULT 0")], null=True)
    id = BigAutoField()
    new_password = CharField(column_name='new_Password', null=True)
    password = CharField(null=True)
    prompt_message = CharField(null=True)
    username = CharField(unique=True)

    class Meta:
        table_name = 'user'

class UserRole(BaseModel):
    role = ForeignKeyField(column_name='role_id', field='id', model=Role)
    user = ForeignKeyField(column_name='user_id', field='id', model=User)

    class Meta:
        table_name = 'user_role'
        primary_key = False

